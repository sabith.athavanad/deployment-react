import React, { useState } from "react";
import ModalComponent from "../components/modal";
import ModalDelete from "../components/modalDelete";
import TableComponent from "../components/table";
import "./Homepage.css";

const Homepage = () => {
  const [visible, setVisible] = useState(false);
  const [shown, setshown] = useState(false);

  return (
    <div className="home-content">
      <button className="deleteButton" onClick={() => setshown(true)}>
        Delete Record
      </button>
      <ModalDelete shown={shown} setshown={setshown} name="deletebutton" />
      <button className="addButton" onClick={() => setVisible(true)}>
        Add New
      </button>
      <ModalComponent
        visible={visible}
        setVisible={setVisible}
        name="addbutton"
      />

      <TableComponent />
    </div>
  );
};

export default Homepage;
