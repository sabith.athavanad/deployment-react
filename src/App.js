import "./App.css";
import Homepage from "./pages/Homepage";
import Header from "./components/common/Header";
import Footer from "./components/common/Footer";
import "antd/dist/antd.css";

function App() {
  return (
    <div className="App">
      <Homepage />
      <Header />
      <Footer />
    </div>
  );
}

export default App;
