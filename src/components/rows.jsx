import React from "react";

const Rows = ({ rows }) => {
  return (
    <tbody>
      {rows.map((element) => {
        return (
          <tr>
            <td>{element.name}</td>
            <td>{element.email}</td>
            <td>{element.mobile}</td>
            <td>{element.address}</td>
            <td>{element.officeAddress}</td>
          </tr>
        );
      })}
    </tbody>
  );
};

export default Rows;
