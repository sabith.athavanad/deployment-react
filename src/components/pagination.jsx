import React from "react";

const Pagination = ({ currentPage, paginate, totalRecords }) => {

  
  return (
    <div>
      
      <button
        className="buttonPrev"
        onClick={() => {
          if (currentPage  > 0) currentPage -= 1;
          else currentPage = totalRecords / 5;
          console.log("CurrentPage is " + currentPage);
          paginate(currentPage);
        }}
      >
        &lt;
      </button>

      <button
        className="buttonNext"
        onClick={() => {
          if (currentPage * 5 < totalRecords) currentPage += 1;
          else currentPage = 1;
          console.log("CurrentPage is " + currentPage);
          paginate(currentPage);
        }}
      >
        &gt;
      </button>
    </div>
  );
};

export default Pagination;
