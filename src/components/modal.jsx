import React from "react";
import { Form, Modal, Input, Button } from "antd";

import "../pages/Homepage.css";

const ModalComponent = ({ visible, setVisible }) => {
  var l = Object.keys(localStorage).length;
  var itemNo = l + 1; //for unique key in local storage
  console.log(itemNo);
  const handleCancel = () => {
    setVisible(false);
  };
  const onFinish = (values) => {
    itemNo += 1;
    localStorage.setItem(itemNo + values.mobile, JSON.stringify(values));
    console.log("Success:" + values);
    setVisible(false);
  };

  return (
    <Modal visible={visible} onCancel={handleCancel} footer={null}>
      <Form
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
      >
        <div>
          <Form.Item
            name="name"
            rules={[
              {
                required: true,
                message: "Please input your Name!",
              },
            ]}
          >
            <Input className="modalInput" placeholder="Name" />
          </Form.Item>
        </div>

        <div>
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                type: "email",
                message: "Please input Valid E-mail...!",
              },
            ]}
          >
            <Input className="modalInput" placeholder="E-mail ID" />
          </Form.Item>
        </div>

        <div>
          <Form.Item
            name="mobile"
            rules={[
              {
                required: true,
                pattern: new RegExp(/^\d{10}$/),
                message: "Please input your Mobile No.!",
              },
            ]}
          >
            <Input className="modalInput" placeholder="Mobile Number" />
          </Form.Item>
        </div>

        <div>
          <Form.Item
            name="address"
            rules={[
              {
                required: true,
                message: "Please input your Address!",
              },
            ]}
          >
            <Input className="modalInput" placeholder="Home Address" />
          </Form.Item>
        </div>

        <div>
          <Form.Item
            name="officeAddress"
            rules={[
              {
                required: true,
                message: "Please input your Office Address!",
              },
            ]}
          >
            <Input className="modalInput" placeholder="Office address" />
          </Form.Item>
        </div>

        <div>
          <Button className="addbutton" htmlType="submit">
            Submit
          </Button>
        </div>
      </Form>
    </Modal>
  );
};

export default ModalComponent;
