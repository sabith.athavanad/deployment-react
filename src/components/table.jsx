import React, { useState, useEffect } from "react";
import Rows from "./rows";
import Pagination from "./pagination";
import "../pages/Homepage.css";

const TableComponent = () => {
  const [rows, setRows] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [rowsPerPage] = useState(5);

  let totalRecords = Object.keys(localStorage).length;

  useEffect(() => {
    var ar = [];
    let items = Object.keys(localStorage); //getting keys from local storage
    for (let i in items) {
      ar.push(JSON.parse(localStorage.getItem(items[i])));
    }
    setRows(ar);
  }, [totalRecords]);

  //get current row
  const indexOfLastRow = currentPage * rowsPerPage;
  const indexOfFirstRow = indexOfLastRow - rowsPerPage;
  const currentRows = rows.slice(indexOfFirstRow, indexOfLastRow);

  //Change Page
  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
    console.log("Page number in function : " + currentPage);
  };

  return (
    <div>
      <table className="content-table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Email-ID</th>
            <th>Mobile Number</th>
            <th>Home Address</th>
            <th>office Address</th>
          </tr>
        </thead>

        <Rows rows={currentRows} />
      </table>
      <div className="pagi-txt">Showing {currentRows.length} out of {totalRecords}</div>
      <Pagination
        currentPage={currentPage}
        paginate={paginate}
        totalRecords={totalRecords}
      />
    </div>
  );
};

export default TableComponent;
