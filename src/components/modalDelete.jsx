import React from "react";
import { Form, Modal, Input, Button } from "antd";
import "../pages/Homepage.css";

const ModalDelete = ({ shown, setshown }) => {
  const totalItems = [];
  Object.keys(localStorage).map((value) => {
    return totalItems.push(JSON.parse(localStorage.getItem(value)));
  });
  //console.log(totalItems);
  const handleCancel = () => {
    setshown(false);
  };
  const onFinish = (values) => {
    totalItems.filter((v, index) => {
      //return ((v["name"]===values["name"]) && (v["email"]===values["email"]) && (v["mobile"]===values["mobile"]) && (v["address"]===values["address"]) && (v["officeAddress"]===values["officeAddress"]))
      if (
        v["name"] === values["name"] &&
        v["email"] === values["email"] &&
        v["mobile"] === values["mobile"] &&
        v["address"] === values["address"] &&
        v["officeAddress"] === values["officeAddress"]
      ) {
        console.log("Success", index + 1);
        localStorage.removeItem(localStorage.key(index));
        return alert("The given data is removed");
      } else return console.log("No data found in the given Details");
    });

    setshown(false);
  };

  return (
    <Modal visible={shown} onCancel={handleCancel} footer={null}>
      <Form
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
      >
        <div>
          <Form.Item
            name="name"
            rules={[
              {
                required: true,
                message: "Please input your Name!",
              },
            ]}
          >
            <Input className="modalInput" placeholder="Name" />
          </Form.Item>
        </div>

        <div>
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                type: "email",
                message: "Please input your E-mail!",
              },
            ]}
          >
            <Input className="modalInput" placeholder="E-mail ID" />
          </Form.Item>
        </div>

        <div>
          <Form.Item
            name="mobile"
            rules={[
              {
                required: true,
                message: "Please input your Mobile No.!",
              },
            ]}
          >
            <Input className="modalInput" placeholder="Mobile Number" />
          </Form.Item>
        </div>

        <div>
          <Form.Item
            name="address"
            rules={[
              {
                required: true,
                message: "Please input your Address!",
              },
            ]}
          >
            <Input className="modalInput" placeholder="Home Address" />
          </Form.Item>
        </div>

        <div>
          <Form.Item
            name="officeAddress"
            rules={[
              {
                required: true,
                message: "Please input your Office Address!",
              },
            ]}
          >
            <Input className="modalInput" placeholder="Office address" />
          </Form.Item>
        </div>

        <div>
          <Button className="deletebutton" htmlType="submit">
            Submit
          </Button>
        </div>
      </Form>
    </Modal>
  );
};

export default ModalDelete;
